
// lưu giá tiền km đầu tiên từng loại xe 
const UBER_CAR = "uberCar"
const UBER_SUV = "uberSUV"
const UBER_BLACK = "uberBlack"

// trả về km đầu tiên theo từng loại xe 
function tinhGiaTienKmDauTien(car){
    if(car== UBER_CAR){
        return 8000;
    }
    if(car == UBER_SUV){
        return 9000;
    }
    if(car==UBER_BLACK){
        return 10000;
    }
}
// tính giá tiền km 1 đến 19
function tinhGiaTienKm1_19(car){
    switch (car){
        case UBER_CAR: {
            return 7500
        }
        case UBER_SUV: {
            return 8500
        }
        case UBER_BLACK: {
            return 9500
        }
        default:
            return 0
    }
}
// tính giá tiền km 19 trở đi
function tinhGiaTienKm19TroDi(car){
    switch (car){
        case UBER_CAR: {
            return 7000
        }
        case UBER_SUV: {
            return 8000
        }
        case UBER_BLACK: {
            return 9000
        }
        default:
            return 0
    }
}
// thời gian chờ 
function tinhTienTheoTgCho(car){
    var tgCho = +document.getElementById("tgCho").value
    var tienTgCho = 0
    if(car== UBER_CAR){
        tienTgCho = Math.floor(tgCho/3)*2000
        return tienTgCho;
    }
    if(car == UBER_SUV){
        tienTgCho = Math.floor(tgCho/3)*3000
        return tienTgCho;
    }
    if(car==UBER_BLACK){
        tienTgCho = Math.floor(tgCho/3)*3500
        return tienTgCho;
    }

}
// main function 
function tinhTienUber(){
    var thanhTien = document.getElementById("divThanhTien")
    var soKm = +document.getElementById("txt-km").value
    var tongTien = 0

    var carOption = document.querySelector('input[name="selector"]:checked').value;
    console.log('carOption: ', carOption);

    var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
    console.log('tinhGiaTienKmDauTien: ', giaTienKmDauTien);

    var giaTienKm1_19 = tinhGiaTienKm1_19(carOption)
    console.log('giaTienKm1_19: ', giaTienKm1_19);

    var giaTienKm19TroDi = tinhGiaTienKm19TroDi(carOption)
    console.log('giaTienKm19TroDi: ', giaTienKm19TroDi);
    
    var tienThoiGianCho = tinhTienTheoTgCho(carOption)
    console.log('tienThoiGianCho: ', tienThoiGianCho);

    if(soKm<=1){
        tongTien = giaTienKmDauTien*soKm + tienThoiGianCho
    }else if(1<soKm<=19){
        tongTien = giaTienKmDauTien + (soKm-1)*giaTienKm1_19 + tienThoiGianCho
    }else{
        tongTien = giaTienKmDauTien + 18*giaTienKm1_19 + (soKm-19)*giaTienKm19TroDi + tienThoiGianCho
    }
    
    thanhTien.classList.remove('d-none')

    document.getElementById("xuatTien").innerHTML= `${tongTien.toLocaleString()} VND`
    
}
